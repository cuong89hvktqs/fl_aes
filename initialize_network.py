from loguru import logger
import torch
import os
from function.arguments import Arguments
from function.nets import AE, VAE


if __name__ == "__main__":
    args = Arguments(logger)

    args.get_logger().debug("Initialize the AE model with the dimension of {}".format(args.dimension))
    full_save_path = os.path.join(args.get_default_model_folder_path(), "AE.model")
    torch.save(AE(args.dimension).state_dict(), full_save_path)

    args.get_logger().debug("Initialize the VAE model with the dimension of {}".format(args.dimension))
    full_save_path = os.path.join(args.get_default_model_folder_path(), "VAE.model")
    torch.save(VAE(args.dimension).state_dict(), full_save_path)
