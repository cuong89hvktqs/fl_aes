from datetime import datetime


def generate_experiment_ids(exp_type, replacement_ratio, num_poisoned_workers):
    """
    Generate the filename for experiment ID.

    :param exp_type: index for experiments
    :param replacement_ratio: ratio
    :param num_poisoned_workers: number of poison client
    """
    log_file = "logs/{}_{}_{}_{}.log".format(
        exp_type,
        str(replacement_ratio),
        str(num_poisoned_workers),
        str(datetime.now()),
    )

    return log_file
