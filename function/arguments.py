from .nets import AE, VAE
import torch

# Setting the seed for Torch
SEED = 1
torch.manual_seed(SEED)


class Arguments:
    def __init__(self, logger):
        self.logger = logger
        # data config
        self.train_batch_size = 8
        self.mal_batch_size = 8
        self.val_batch_size = 128
        self.test_batch_size = 8
        self.dimension = 122

        # experiment config
        self.num_workers = 200
        self.epochs = 10000
        self.es_offset = 500
        self.cuda = True
        self.shuffle = False

        self.loss_function = torch.nn.MSELoss
        self.default_model_folder_path = "default_models"

        self.dataset = "nsl_kdd"
        # cic_ids, nb_iot, nsl_kdd,  nsl_kdd_one_class, unsw, unsw_big, unsw_one_class, spambase, ctu13_08 09 10 13, internet_ad

        self.train_data_loader_pickle_path = "data_loaders/{}/train_data_loader.pickle".format(self.dataset)
        self.val_data_loader_pickle_path = "data_loaders/{}/val_data_loader.pickle".format(self.dataset)
        self.test_data_loader_pickle_path = "data_loaders/{}/test_data_loader.pickle".format(self.dataset)
        self.mal_data_loader_pickle_path = "data_loaders/{}/mal_data_loader.pickle".format(self.dataset)

        # exp loop
        self.exp_type = ""
        self.noise_type = ""
        self.learning_rate = 0
        self.shrink_factor = 0
        self.threshold_factor = 0
        self.std_noise = 0
        self.num_poisoned_workers = 0
        self.replacement_ratio = 0

    def get_train_batch_size(self):
        return self.train_batch_size

    def get_val_batch_size(self):
        return self.val_batch_size

    def get_test_batch_size(self):
        return self.test_batch_size

    def get_mal_batch_size(self):
        return self.mal_batch_size

    def set_dimension(self, dimension):
        self.dimension = dimension

    def get_dimension(self):
        return self.dimension

    def set_num_workers(self, num_workers):
        self.num_workers = num_workers

    def get_num_workers(self):
        return self.num_workers

    def get_es_offset(self):
        return self.es_offset

    def get_net(self, exp_type):
        if exp_type == "VAE":
            return VAE
        else:
            return AE

    def get_loss_function(self):
        return self.loss_function

    def set_train_data_loader_pickle_path(self, path):
        self.train_data_loader_pickle_path = path

    def get_train_data_loader_pickle_path(self):
        return self.train_data_loader_pickle_path

    def set_val_data_loader_pickle_path(self, path):
        self.val_data_loader_pickle_path = path

    def get_val_data_loader_pickle_path(self):
        return self.val_data_loader_pickle_path

    def set_test_data_loader_pickle_path(self, path):
        self.test_data_loader_pickle_path = path

    def get_test_data_loader_pickle_path(self):
        return self.test_data_loader_pickle_path

    def set_mal_data_loader_pickle_path(self, path):
        self.mal_data_loader_pickle_path = path

    def get_mal_data_loader_pickle_path(self):
        return self.mal_data_loader_pickle_path

    def get_cuda(self):
        return self.cuda

    def get_default_model_folder_path(self):
        return self.default_model_folder_path

    def get_num_epochs(self):
        return self.epochs

    def get_logger(self):
        return self.logger

    def get_num_workers(self):
        return self.num_workers

    def get_shuffle(self):
        return self.shuffle

    def set_exp_type(self, exp_type):
        self.exp_type = exp_type

    def get_exp_type(self):
        return self.exp_type

    def set_noise_type(self, noise_type):
        self.noise_type = noise_type

    def get_noise_type(self):
        return self.noise_type

    def set_learning_rate(self, learning_rate):
        self.learning_rate = learning_rate

    def get_learning_rate(self):
        return self.learning_rate

    def set_shrink_factor(self, shrink_factor):
        self.shrink_factor = shrink_factor

    def get_shrink_factor(self):
        return self.shrink_factor

    def set_threshold_factor(self, threshold_factor):
        self.threshold_factor = threshold_factor

    def get_threshold_factor(self):
        return self.threshold_factor

    def set_std_noise(self, std_noise):
        self.std_noise = std_noise

    def get_std_noise(self):
        return self.std_noise

    def set_num_poisoned_workers(self, num_poisoned_workers):
        self.num_poisoned_workers = num_poisoned_workers

    def get_num_poisoned_workers(self):
        return self.num_poisoned_workers

    def set_replacement_ratio(self, replacement_ratio):
        self.replacement_ratio = replacement_ratio

    def get_replacement_ratio(self):
        return self.replacement_ratio

    def log(self):
        """
        Log this arguments object to the logger.
        """
        self.logger.debug("Arguments: {}", str(self))

    def __str__(self):
        return (
            "\nExp Type: {}\n".format(self.exp_type)
            + "Noise Type: {}\n".format(self.noise_type)
            + "Learning Rate: {}\n".format(self.learning_rate)
            + "Shrink Factor: {}\n".format(self.shrink_factor)
            + "Threshold Factor: {}\n".format(self.threshold_factor)
            + "Noise STD: {}\n".format(self.std_noise)
            + "Number of Poisoned Clients: {}\n".format(self.num_poisoned_workers)
            + "Replacement Ratio: {}\n".format(self.replacement_ratio)
            + "\n"
            + "Train Batch Size: {}\n".format(self.train_batch_size)
            + "Val Batch Size: {}\n".format(self.val_batch_size)
            + "Test Batch Size: {}\n".format(self.test_batch_size)
            + "Mal Batch Size: {}\n".format(self.mal_batch_size)
            + "Dimension: {}\n".format(self.dimension)
            + "Epochs: {}\n".format(self.epochs)
            + "Offsets: {}\n".format(self.es_offset)
            + "Number of Workers: {}\n".format(self.num_workers)
            + "Dataset: {}\n".format(self.dataset)
            + "Loss Function: {}\n".format(self.loss_function)
        )
