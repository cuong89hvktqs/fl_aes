import torch
import torch.optim as optim
from sklearn.metrics import classification_report, roc_auc_score, confusion_matrix
import os
import numpy as np
import copy
from matplotlib import pyplot as plt


class Client:
    def __init__(self, args, client_idx, train_data_loader, val_data_loader, test_data_loader):
        """
        :param args: experiment arguments
        :type args: Arguments
        :param client_idx: Client index
        :type client_idx: int
        :param train_data_loader: Training data loader
        :type train_data_loader: torch.utils.data.DataLoader
        :param val_data_loader: Val data loader
        :type val_data_loader: torch.utils.data.DataLoader
        :param test_data_loader: Test data loader
        :type test_data_loader: torch.utils.data.DataLoader
        """
        self.args = args
        self.client_idx = client_idx
        self.exp_type = self.args.get_exp_type()

        self.device = self.initialize_device()
        self.set_net(self.load_default_model())

        self.loss_function = self.args.get_loss_function()()
        self.optimizer = optim.Adam(self.net.parameters(), lr=self.args.get_learning_rate())

        self.train_data_loader = train_data_loader
        self.val_data_loader = val_data_loader
        self.test_data_loader = test_data_loader

    def initialize_device(self):
        """
        Creates appropriate torch device for client operation.
        """
        if torch.cuda.is_available() and self.args.get_cuda():
            # print("Device: GPU")
            return torch.device("cuda:0")
        else:
            # print("Device: CPU")
            return torch.device("cpu")

    def set_net(self, net):
        """
        Set the client's NN.

        :param net: torch.nn
        """
        self.net = net
        self.net.to(self.device)

    def load_default_model(self):
        """
        Load a model from default model file.

        This is used to ensure consistent default model behavior.
        """
        model_class = self.args.get_net(self.exp_type)
        default_model_path = os.path.join(self.args.get_default_model_folder_path(), model_class.__name__ + ".model")

        return self.load_model_from_file(default_model_path)

    def load_model_from_file(self, model_file_path):
        """
        Load a model from a file.

        :param model_file_path: string
        """
        model_class = self.args.get_net(self.exp_type)
        model = model_class()

        if os.path.exists(model_file_path):
            try:
                model.load_state_dict(torch.load(model_file_path))
            except:
                self.args.get_logger().warning(
                    "Couldn't load model. Attempting to map CUDA tensors to CPU to solve error."
                )

                model.load_state_dict(torch.load(model_file_path, map_location=torch.device("cpu")))
        else:
            self.args.get_logger().warning("Could not find model: {}".format(model_file_path))

        return model

    def get_client_index(self):
        """
        Returns the client index.
        """
        return self.client_idx

    def get_nn_parameters(self):
        """
        Return the NN's parameters.
        """
        return self.net.state_dict()

    def update_nn_parameters(self, new_params):
        """
        Update the NN's parameters.

        :param new_params: New weights for the neural network
        :type new_params: dict
        """
        self.net.load_state_dict(copy.deepcopy(new_params), strict=True)

    def calculate_loss(self, input):
        if self.exp_type == "AE":
            _, decode = self.net(input)
            return self.loss_function(decode, input)

        if self.exp_type == "DAE":
            corrupted_input = input + ((self.args.get_std_noise() ** 0.5) * torch.randn(input.shape)).to(self.device)
            _, decode = self.net(corrupted_input)
            return self.loss_function(decode, input)

        if self.exp_type == "VAE":
            _, decode = self.net(input)
            loss1 = self.loss_function(decode, input)
            loss2 = self.net.enc.kl
            return loss1 + loss2

        if self.exp_type == "SAE":
            encode, decode = self.net(input)
            loss1 = self.loss_function(decode, input)

            zero_m = torch.zeros(encode.shape).to(self.device)
            loss2 = self.loss_function(encode, zero_m)
            return loss1 + 10 * loss2

        if self.exp_type == "SDAE":
            corrupted_input = input + ((self.args.get_std_noise() ** 0.5) * torch.randn(input.shape)).to(self.device)
            encode, decode = self.net(corrupted_input)
            loss1 = self.loss_function(decode, input)

            zero_m = torch.zeros(encode.shape).to(self.device)
            loss2 = self.loss_function(encode, zero_m)
            return loss1 + (int)(loss1 / loss2) * loss2

    def train(self):
        self.net.train()
        for (input, _) in self.train_data_loader:
            input = input.to(self.device)
            self.optimizer.zero_grad()
            loss = self.calculate_loss(input)
            loss.backward()
            self.optimizer.step()

    def test_on_data(self, epoch, threshold, isVal):
        truth = []
        output = []
        if isVal:
            self.args.get_logger().debug(
                "VALIDATION == Model {} == Epoch {} == Client {}".format(self.exp_type, epoch, self.client_idx)
            )
            for (inputs, labels) in self.val_data_loader:
                inputs, labels = inputs.to(self.device), labels.to(self.device)

                if self.exp_type == "AE" or self.exp_type == "VAE" or self.exp_type == "DAE":
                    _, decode = self.net(inputs)
                    re = ((decode - inputs) ** 2).mean(1)
                    # re = self.loss_function(outputs, inputs)
                if self.exp_type == "SAE" or self.exp_type == "SDAE":
                    latent, decode = self.net(inputs)
                    re = (latent**2).sum(1).sqrt()
                    # self.args.get_logger().debug("Val RE:\n" + str(re))
                    re1 = ((decode - inputs) ** 2).mean(1)
                    # self.args.get_logger().debug("Val RE 1:\n" + str(re))

                threshold = torch.mean(re) + self.args.get_threshold_factor() * torch.std(re)
                # threshold = 0.01
                # self.args.get_logger().debug("Val Threshold:\n" + str(threshold))
                prediction = ((re > threshold) * 1).tolist()
                groundtruth = labels.tolist()
                truth = truth + groundtruth
                output = output + prediction

        else:
            self.args.get_logger().debug(
                "TEST == Model {} == Epoch {} == Client {}".format(self.exp_type, epoch, self.client_idx)
            )
            for (inputs, labels) in self.test_data_loader:
                inputs, labels = inputs.to(self.device), labels.to(self.device)

                if self.exp_type == "AE" or self.exp_type == "VAE" or self.exp_type == "DAE":
                    _, decode = self.net(inputs)
                    re = ((decode - inputs) ** 2).mean(1)
                    # re = self.loss_function(outputs, inputs)
                if self.exp_type == "SAE" or self.exp_type == "SDAE":
                    latent, decode = self.net(inputs)
                    re = (latent**2).sum(1).sqrt()
                    # self.args.get_logger().debug("Test RE:\n" + str(re))
                    re1 = ((decode - inputs) ** 2).mean(1)
                    # self.args.get_logger().debug("Test RE 1:\n" + str(re))

                threshold = torch.mean(re) + self.args.get_threshold_factor() * torch.std(re)
                # threshold = 0.01
                # self.args.get_logger().debug("Test Threshold:\n" + str(threshold))
                prediction = ((re > threshold) * 1).tolist()
                groundtruth = labels.tolist()
                truth = truth + groundtruth
                output = output + prediction

        confusion_mat = confusion_matrix(truth, output)
        roc = roc_auc_score(truth, output)

        self.args.get_logger().debug("Classification Report:\n" + classification_report(truth, output))
        self.args.get_logger().debug("Confusion Matrix:\n" + str(confusion_mat))
        self.args.get_logger().debug("ROC AUC Score: {}".format(roc))
        return roc

    def test(self, epoch, isVal=False):
        self.net.eval()
        with torch.no_grad():
            threshold = 0.01
            roc = self.test_on_data(epoch, threshold, isVal)
            # self.visualize(epoch)
            return roc

    def visualize(self, epoch):
        self.net.eval()

        with torch.no_grad():
            benign_X = []
            benign_y = []
            mal_X = []
            mal_y = []

            for (inputs, labels) in self.test_data_loader:
                inputs, labels = inputs.to(self.device), labels.to(self.device)

                encode, _ = self.net(inputs)

                list_label = labels.tolist()
                list_encode = encode.tolist()

                for i in range(len(list_label)):
                    if list_label[i] == 1:
                        mal_X.append(list_encode[i][0])
                        mal_y.append(list_encode[i][1])
                    else:
                        benign_X.append(list_encode[i][0])
                        benign_y.append(list_encode[i][1])

            plt.figure()
            plt.scatter(mal_X, mal_y, c="red", marker="x")
            plt.scatter(benign_X, benign_y, c="blue", marker="o")
            plt.savefig("./visual/{}/epoch_{}_{}.pdf".format(self.exp_type, epoch, self.exp_type))
            plt.close()
