import random
from loguru import logger
from agents.client import Client
from function.arguments import Arguments
from function.utils import (
    generate_data_loaders_from_distributed_dataset,
    distribute_batches_equally,
    average_nn_parameters,
    convert_distributed_data_into_numpy,
    poison_data,
    identify_random_elements,
    load_train_data_loader,
    load_val_data_loader,
    load_test_data_loader,
    load_mal_data_loader,
    generate_experiment_ids,
)


def train_subset_of_clients(epoch, args, clients):
    """
    Train a subset of clients per round.
    :param epoch: epoch
    :type epoch: int
    :param args: arguments
    :type args: Arguments
    :param clients: clients
    :type clients: list(Client)
    """

    random_workers = random.sample(list(range(args.get_num_workers())), args.get_num_workers())
    # random_workers = [0]
    for client_idx in random_workers:
        args.get_logger().info(
            "Training {} model epoch #{} on client #{}",
            args.get_exp_type(),
            str(epoch),
            str(clients[client_idx].get_client_index()),
        )
        clients[client_idx].train()

    args.get_logger().info("Averaging client parameters in {} clients".format(len(random_workers)))
    parameters = [clients[client_idx].get_nn_parameters() for client_idx in random_workers]
    new_nn_params = average_nn_parameters(parameters)

    for client in clients:
        # args.get_logger().info("Updating parameters on client #{}", str(client.get_client_index()))
        client.update_nn_parameters(new_nn_params)

    return random_workers


def create_clients(args, train_data_loaders, val_data_loader, test_data_loader):
    """
    Create a set of clients.
    """
    clients = []
    for idx in range(args.get_num_workers()):
        clients.append(Client(args, idx, train_data_loaders[idx], val_data_loader, test_data_loader))

    return clients


def run_machine_learning(clients, val_client, test_client, args):
    """
    Complete machine learning over a series of clients.
    """
    max_roc = 0.0
    offset = 0
    max_test_roc = 0.0

    for epoch in range(1, args.get_num_epochs() + 1):
        workers_selected = train_subset_of_clients(epoch, args, clients)
        params = clients[workers_selected[0]].get_nn_parameters()

        val_client.update_nn_parameters(params)
        val = val_client.test(epoch, isVal=True)

        if val > max_roc:
            offset = 0
            max_roc = val
            test_client.update_nn_parameters(params)
            max_test_roc = test_client.test(epoch)

        else:
            offset += 1
            if offset >= args.get_es_offset():
                args.get_logger().info("Early stopping at epoch #{}, the  best score: {}", epoch, max_roc)
                return max_test_roc
    return max_test_roc


def run_exp(exp_type, learning_rate, noise_type, std_noise, num_poisoned_workers, replacement_ratio, client_id=0):
    log_file = generate_experiment_ids(exp_type, replacement_ratio, num_poisoned_workers)

    # Initialize logger
    handler = logger.add(log_file, enqueue=True)

    args = Arguments(logger)
    args.set_exp_type(exp_type)
    args.set_learning_rate(learning_rate)
    args.set_noise_type(noise_type)
    args.set_std_noise(std_noise)
    args.set_num_poisoned_workers(num_poisoned_workers)
    args.set_replacement_ratio(replacement_ratio)
    args.log()

    train_data_loader = load_train_data_loader(logger, args)
    val_data_loader = load_val_data_loader(logger, args)
    test_data_loader = load_test_data_loader(logger, args)
    mal_data_loader = load_mal_data_loader(logger, args)

    # Distribute batches equal volume IID
    distributed_train_dataset = distribute_batches_equally(train_data_loader, args.get_num_workers())
    distributed_train_dataset = convert_distributed_data_into_numpy(distributed_train_dataset)

    poisoned_workers = identify_random_elements(args.get_num_workers(), args.get_num_poisoned_workers())
    distributed_train_dataset = poison_data(
        logger,
        distributed_train_dataset,
        args.get_num_workers(),
        poisoned_workers,
        noise_type,
        mal_data_loader,
        replacement_ratio,
    )

    train_data_loaders = generate_data_loaders_from_distributed_dataset(
        distributed_train_dataset, args.get_train_batch_size()
    )

    clients = create_clients(args, train_data_loaders, val_data_loader, test_data_loader)
    # clients = [Client(args, client_id, train_data_loaders[client_id], val_data_loader, test_data_loader)]
    val_client = Client(args, args.get_num_workers() + 1, train_data_loader, val_data_loader, test_data_loader)
    test_client = Client(args, args.get_num_workers() + 2, train_data_loader, val_data_loader, test_data_loader)

    max_test_roc = run_machine_learning(clients, val_client, test_client, args)

    # file1 = open("myfile2.txt", "a")
    # file1.write("{}, {}\n".format(client_id, max_test_roc))
    # file1.close()

    logger.remove(handler)
