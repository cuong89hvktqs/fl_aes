import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def num_ie():
    plt.figure()
    x = ("10", "20", "30", "40")
    delay = [0.86, 1.73, 2.68, 3.37]
    energy = [
        25.67,
        51.33,
        81.13,
        105.23,
    ]
    # Create figure and axis #1
    fig, ax1 = plt.subplots()
    # plot line chart on axis #1
    ax1.bar(x, delay, width=0.5, color="orange")
    ax1.grid(False)
    ax1.set_ylabel("Delay (s)")
    ax1.set_xlabel("Number of IEs")
    ax1.set_ylim(0, 4)
    ax1.legend(["Delay"], loc="upper left")

    # set up the 2nd axis

    ax2 = ax1.twinx()
    # plot bar chart on axis #2
    ax2.plot(x, energy, marker="o", linewidth=2, markersize=12)
    ax2.set_ylabel("Energy Consumption (J)")
    ax2.set_ylim(0, 120)
    ax2.legend(["Energy Consumption"], loc="upper right")
    plt.savefig("num-ie.pdf")
    plt.close()


def weight():
    plt.figure()
    x = ("1:9", "5:5", "7:3", "9:1")
    delay = [9.37, 1.26, 0.86, 0.66]
    energy = [
        10.69,
        18.67,
        25.67,
        87.03,
    ]
    # Create figure and axis #1
    fig, ax1 = plt.subplots()
    # plot line chart on axis #1
    ax1.grid(False)
    ax1.plot(x, delay, marker="o", linewidth=2, markersize=12, color="red")
    ax1.set_ylabel("Delay (s)")
    ax1.set_xlabel("Energy-delay weight")
    ax1.set_ylim(0, 12)
    ax1.legend(["Delay"], loc="upper left")

    # set up the 2nd axis

    ax2 = ax1.twinx()
    # plot bar chart on axis #2
    ax2.plot(x, energy, marker="s", linewidth=2, markersize=12)
    ax2.set_ylabel("Energy Consumption (J)")
    ax2.set_ylim(0, 120)
    ax2.legend(["Energy Consumption"], loc="upper right")

    plt.savefig("weight.pdf")
    plt.close()


def weight_cost():
    plt.figure()

    species = ("1:9", "5:5", "7:3", "9:1")
    weight_counts = {
        "Energy": np.array([36.64, 33.61, 27.73, 31.33]),
        "Delay": np.array([85.66, 57.61, 55.02, 54.77]),
    }
    width = 0.5

    fig, ax = plt.subplots()
    bottom = np.zeros(4)

    for boolean, weight_count in weight_counts.items():
        p = ax.bar(species, weight_count, width, label=boolean, bottom=bottom)
        bottom += weight_count

    ax.legend(loc="upper right")
    ax.set_xlabel("Energy-delay weight")
    ax.set_ylabel("Energy-delay cost")
    """
    x = ("1:9", "5:5", "7:3", "9:1")
    delay = [9.37, 1.26, 0.86, 0.66]
    # Create figure and axis #1
    fig, ax1 = plt.subplots()
    # plot line chart on axis #1
    ax1.grid(False)
    ax1.plot(x, delay, marker="o", linewidth=2, markersize=12, color="red")
    ax1.set_ylabel("Delay (s)")
    ax1.set_xlabel("Energy-delay weight")
    ax1.set_ylim(0, 12)
    ax1.legend(["Delay"], loc="upper left")

    # set up the 2nd axis

    ax2 = ax1.twinx()
    # plot bar chart on axis #2
    ax2.plot(x, energy, marker="s", linewidth=2, markersize=12)
    ax2.set_ylabel("Energy Consumption (J)")
    ax2.set_ylim(0, 120)
    ax2.legend(["Energy Consumption"], loc="upper right")
    """
    plt.savefig("weight_cost.pdf")
    plt.close()


def energy_delay():
    plt.figure()
    species = (
        "RRA",
        "URA",
        "DDPG-PER",
    )
    test = 10

    c10 = [340.9244, 144.8616, 134.8426, 322.3888, 120.9956, 105.8988, 304.2528, 97.171, 82.7464]
    c20 = [657.6116, 255.6466, 229.6712, 610.5356, 227.2058, 198.9662, 574.1582, 203.3834, 166.1218]
    c30 = [1062.3604, 404.223, 312.6484, 1018.5024, 358.3008, 321.0608, 860.1696, 310.1268, 259.0868]
    c40 = [1319.6452, 559.1196, 461.8816, 1282.4012, 510.6094, 418.1908, 1232.4744, 397.28, 324.7824]

    if test == 10:
        cost = c10
    elif test == 20:
        cost = c20
    elif test == 30:
        cost = c30
    else:
        cost = c40

    penguin_means = {
        "IE": (cost[0], cost[3], cost[6]),
        "Dedicated MEC": (cost[1], cost[4], cost[7]),
        "MEC Federation": (cost[2], cost[5], cost[8]),
    }

    x = np.arange(len(species))  # the label locations
    width = 0.25  # the width of the bars
    multiplier = 0

    fig, ax = plt.subplots(layout="constrained")

    for attribute, measurement in penguin_means.items():
        offset = width * multiplier
        ax.bar(x + offset, measurement, width, label=attribute)
        multiplier += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel("Energy-Delay Cost")
    ax.set_xticks(x + width, species)
    ax.legend(loc="upper left", ncols=3)
    ax.set_ylim(0, max(cost) * 1.1)
    plt.savefig("cost-{}.pdf".format(test))
    plt.close()


energy_delay()
