from agents.server import run_exp

if __name__ == "__main__":
    EXP_TYPE = ["SDAE"]  # ['AE', 'DAE', 'VAE', 'SAE', 'SDAE']
    LEARNING_RATE = [1e-5]  # [1e-3, 1e-4]
    NOISE_TYPE = ["gaussian_noise"]  # ["label_flipping", "gaussian_noise"]
    STD_NOISE = [1e-6]  # [1e-2, 1e-3, 1e-4]
    NUM_POISONED_WORKERS = [0, 5, 10]  # 0, 20, 40, 60, 80
    REPLACEMENT_RATIO = [0.05, 0.1, 0.2]  # 0.25, 0.5, 0.75, 1

    for exp_type in EXP_TYPE:
        for lr in LEARNING_RATE:
            for noise_type in NOISE_TYPE:
                for std_noise in STD_NOISE:
                    for num_poison in NUM_POISONED_WORKERS:
                        if num_poison == 0:
                            run_exp(exp_type, lr, noise_type, std_noise, num_poison, 0)
                        else:
                            for rep_rat in REPLACEMENT_RATIO:
                                run_exp(exp_type, lr, noise_type, std_noise, num_poison, rep_rat)
