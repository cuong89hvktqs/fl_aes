## Installation

1. Create a virtualenv (Python 3.7)
   `pip3 install virtualenv`
   `python3 -m venv env`
   `source env/bin/activate`
   maybe need upgrade `python3 -m pip install --upgrade pip`
2. Install dependencies inside of virtualenv (`pip install -r requirements.pip`)
3. If you are planning on using the defense, you will need to install `matplotlib`. This is not required for running experiments, and is not included in the requirements file

### Setup

Before you can run any experiments, you must complete some setup:

1. `python3 initialize_env.py` This command generate the data loader and models.
   If only need to regenerate the network model use the command below:
2. `python3 initialize_network.py`

### General Information

Some pointers & general information:

- Most hyperparameters can be set in the `function/arguments.py` file
- Most specific experiment settings are located in the respective experiment files (see the following sections)

### Experiments

Running an attack: `python3 main.py`
