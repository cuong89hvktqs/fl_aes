import matplotlib.pyplot as plt


def get_auc(file_path):
    file1 = open(file_path, "r")
    lines = file1.readlines()
    list_auc = []
    check = 0

    for line in lines:
        if "VALIDATION ==" in line:
            check = 1
        if check == 1 and "ROC AUC Score" in line:
            spl = line.split("ROC AUC Score: ")
            auc = float(spl[1][: len(spl) - 3])
            list_auc.append(auc)
            check = 0
    return list_auc


auc_ae_00 = get_auc("./logs/AE_0_0_2023-03-26 17:28:00.807122.log")
auc_dae_00 = get_auc("./logs/DAE_0_0_2023-03-26 18:02:21.385345.log")
auc_vae_00 = get_auc("./logs/VAE_0_0_2023-03-26 17:28:16.742480.log")
auc_sae_00 = get_auc("./logs/SAE_0_0_2023-03-26 18:11:43.280399.log")
auc_sdae_00 = get_auc("./logs/SDAE_0_0_2023-03-26 17:28:23.507993.log")

plt.figure()
plt.ylabel("AUC")
plt.xlabel("Epochs")
plt.plot(auc_ae_00, color="black", label="AE")
plt.plot(auc_dae_00, color="blue", label="DAE")
plt.plot(auc_vae_00, color="green", label="VAE")
plt.plot(auc_sae_00, color="red", label="SAE")
plt.plot(auc_sdae_00, color="purple", label="SDAE")
plt.legend(loc="lower right")
plt.savefig("test.pdf")
plt.close()


auc_ae_180 = get_auc("./logs/nbiot_200_gn_lr1e-5/AE/AE_1_80_2023-03-18 18:34:35.094121.log")
auc_dae_180 = get_auc("./logs/nbiot_200_gn_lr1e-5/DAE/DAE_1_80_2023-03-19 03:59:57.105283.log")
auc_vae_180 = get_auc("./logs/nbiot_200_gn_lr1e-5/VAE/VAE_1_80_2023-03-19 21:39:07.271779.log")
auc_sae_180 = get_auc("./logs/nbiot_200_gn_lr1e-5/SAE/SAE_1_80_2023-03-18 18:33:30.830195.log")
auc_sdae_180 = get_auc("./logs/nbiot_200_gn_lr1e-5/SDAE/SDAE_1_80_2023-03-18 18:30:56.645195.log")

plt.figure()
plt.ylabel("AUC")
plt.xlabel("Epochs")
plt.plot(auc_ae_180, color="black", label="AE")
plt.plot(auc_dae_180, color="blue", label="DAE")
plt.plot(auc_vae_180, color="green", label="VAE")
plt.plot(auc_sae_180, color="red", label="SAE")
plt.plot(auc_sdae_180, color="purple", label="SDAE")
plt.legend(loc="lower right")
plt.savefig("test_180.pdf")
plt.close()

auc_ae_2520 = get_auc("./logs/nbiot_200_gn_lr1e-5/AE/AE_0.25_20_2023-03-19 14:57:52.504958.log")
auc_dae_2520 = get_auc("./logs/nbiot_200_gn_lr1e-5/DAE/DAE_0.25_20_2023-03-19 00:19:42.130643.log")
auc_vae_2520 = get_auc("./logs/nbiot_200_gn_lr1e-5/VAE/VAE_0.25_20_2023-03-19 05:47:26.074775.log")
auc_sae_2520 = get_auc("./logs/nbiot_200_gn_lr1e-5/SAE/SAE_0.25_20_2023-03-19 14:30:48.412714.log")
auc_sdae_2520 = get_auc("./logs/nbiot_200_gn_lr1e-5/SDAE/SDAE_0.25_20_2023-03-19 21:57:49.909057.log")

plt.figure()
plt.ylabel("AUC")
plt.xlabel("Epochs")
plt.plot(auc_ae_2520, color="black", label="AE")
plt.plot(auc_dae_2520, color="blue", label="DAE")
plt.plot(auc_vae_2520, color="green", label="VAE")
plt.plot(auc_sae_2520, color="red", label="SAE")
plt.plot(auc_sdae_2520, color="purple", label="SDAE")
plt.legend(loc="lower right")
plt.savefig("test_2520.pdf")
plt.close()
