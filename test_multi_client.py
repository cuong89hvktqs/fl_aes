from federated_learning.worker_selection import RandomSelectionStrategy
from agents.server import run_exp


if __name__ == "__main__":
    # ['AE', 'DAE', 'VAE', 'SAE', 'SDAE']
    EXP_TYPE = ["AE"]
    NUM_POISONED_WORKERS = [0]  # 1,5,10,15%
    REPLACEMENT_RATIO = [0.25, 0.5, 0.75, 1]
    KWARGS = {"NUM_WORKERS_PER_ROUND": 1000}

    for exp_type in EXP_TYPE:
        for num_poison in NUM_POISONED_WORKERS:
            if num_poison == 0:
                for client_id in range(800, 1000):
                    KWARGS = {"NUM_WORKERS_PER_ROUND": 1000, "client_id": client_id}
                    run_exp(0, num_poison, KWARGS, RandomSelectionStrategy(), exp_type)
            else:
                for rep_rat in REPLACEMENT_RATIO:
                    run_exp(rep_rat, num_poison, KWARGS, RandomSelectionStrategy(), exp_type)
