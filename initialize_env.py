from loguru import logger
import torch
import pathlib
import os
from function.arguments import Arguments
from function.datasets import DataReader
from function.nets import AE, VAE
from function.utils import (
    generate_train_loader,
    generate_val_loader,
    generate_test_loader,
    generate_mal_loader,
    save_data_loader_to_file,
)


if __name__ == "__main__":
    args = Arguments(logger)
    dataset = DataReader(args, args.dataset)
    if not os.path.exists("data_loaders/{}".format(args.dataset)):
        pathlib.Path("data_loaders/{}".format(args.dataset)).mkdir(parents=True, exist_ok=True)

    train_data_loader = generate_train_loader(args, dataset)
    val_data_loader = generate_val_loader(args, dataset)
    test_data_loader = generate_test_loader(args, dataset)
    mal_data_loader = generate_mal_loader(args, dataset)

    with open(args.train_data_loader_pickle_path, "wb") as f:
        save_data_loader_to_file(train_data_loader, f)

    with open(args.val_data_loader_pickle_path, "wb") as f:
        save_data_loader_to_file(val_data_loader, f)

    with open(args.test_data_loader_pickle_path, "wb") as f:
        save_data_loader_to_file(test_data_loader, f)

    with open(args.mal_data_loader_pickle_path, "wb") as f:
        save_data_loader_to_file(mal_data_loader, f)

    # -------------------------------------------
    # ----------- Model Initialization ----------
    # -------------------------------------------
    if not os.path.exists(args.get_default_model_folder_path()):
        os.mkdir(args.get_default_model_folder_path())

    args.get_logger().debug("Initialize the AE model with the dimension of {}".format(args.dimension))
    full_save_path = os.path.join(args.get_default_model_folder_path(), "AE.model")
    torch.save(AE(args.dimension).state_dict(), full_save_path)

    args.get_logger().debug("Initialize the VAE model with the dimension of {}".format(args.dimension))
    full_save_path = os.path.join(args.get_default_model_folder_path(), "VAE.model")
    torch.save(VAE(args.dimension).state_dict(), full_save_path)
